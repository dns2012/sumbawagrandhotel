<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'default' );

/** MySQL database username */
define( 'DB_USER', 'default' );

/** MySQL database password */
define( 'DB_PASSWORD', 'secret' );

/** MySQL hostname */
define( 'DB_HOST', 'mysql' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '7|Oq2(X&]s4<,g2aHmvs]#uS7!b<[D1gRN$3*>J]3EXJzF7:4/0Jq9Z!h]VK^Ii:' );
define( 'SECURE_AUTH_KEY',  '-tbdx;xU#?lK^e/n+ 1?*}6JK&$D4L>FWy 8M&k4rg<u^vIrk1~}eZTktv=5*J%o' );
define( 'LOGGED_IN_KEY',    '#~9..sMahm vbOOL1aTLs<^6)mxfT`a; h<vu!%t&iN%-cQ^I)^L&{A0z6baJFD<' );
define( 'NONCE_KEY',        'A1(kW]FU *l?#@Ng4Hs@zuIy; A>>B@,z{T0SR5xVNeEz5:eHqw9oLj!))>jW#U7' );
define( 'AUTH_SALT',        'TwF2N%?+z8u(ANhDuc};j,mA/ U/4l#|K$9GuTPtW=rZ54]vX7MlI[B4PjyQ;?&?' );
define( 'SECURE_AUTH_SALT', 'Gl: c1bSgnIfDsj[6BU$GT`,JtthTo/;&M E_>zE:jQ$DIIixN%z@~hVzX}(FJO!' );
define( 'LOGGED_IN_SALT',   'ze_TJf/$qEp6:VuHZ5!(3PcL3Qh?Py[;ledFp[~N=17&U$}@Vj]R$Z.0|XGM=uDV' );
define( 'NONCE_SALT',       'L!{6y9fNlR})mfXN4,k_A^KBKtO9)x:@~ 6->aXHe1SF^E7Us;Qc;hKd[LCs$LN&' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

@ini_set(‘upload_max_size’ , ‘256M’ );

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
